package com.udea.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniel Jaramillo
 */
@Entity
@XmlRootElement
public class Weather implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private double temperatura;
  private double porcentaje_hum_rel;
  private String tipo_nubosidad;
  private double prob_precipitaciones;
  private Date dia;
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  /**
   * Get the value of temperatura
   *
   * @return the value of temperatura
   */
  public double getTemperatura() {
    return temperatura;
  }

  /**
   * Set the value of temperatura
   *
   * @param temperatura new value of temperatura
   */
  public void setTemperatura(double temperatura) {
    this.temperatura = temperatura;
  }
  
  /**
   * Get the value of porcentaje_hum_rel
   *
   * @return the value of porcentaje_hum_rel
   */
  public double getPorcentaje_hum_rel() {
    return porcentaje_hum_rel;
  }

  /**
   * Set the value of porcentaje_hum_rel
   *
   * @param porcentaje_hum_rel new value of porcentaje_hum_rel
   */
  public void setPorcentaje_hum_rel(double porcentaje_hum_rel) {
    this.porcentaje_hum_rel = porcentaje_hum_rel;
  }

  /**
   * Get the value of tipo_nubosidad
   *
   * @return the value of tipo_nubosidad
   */
  public String getTipo_nubosidad() {
    return tipo_nubosidad;
  }

  /**
   * Set the value of tipo_nubosidad
   *
   * @param tipo_nubosidad new value of tipo_nubosidad
   */
  public void setTipo_nubosidad(String tipo_nubosidad) {
    this.tipo_nubosidad = tipo_nubosidad;
  }

  /**
   * Get the value of prob_precipitaciones
   *
   * @return the value of prob_precipitaciones
   */
  public double getProb_precipitaciones() {
    return prob_precipitaciones;
  }

  /**
   * Set the value of prob_precipitaciones
   *
   * @param prob_precipitaciones new value of prob_precipitaciones
   */
  public void setProb_precipitaciones(double prob_precipitaciones) {
    this.prob_precipitaciones = prob_precipitaciones;
  }

  /**
   * Get the value of dia
   *
   * @return the value of dia
   */
  public Date getDia() {
    return dia;
  }

  /**
   * Set the value of dia
   *
   * @param dia new value of dia
   */
  public void setDia(Date dia) {
    this.dia = dia;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Weather)) {
      return false;
    }
    Weather other = (Weather) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "com.udea.modelo.Weather[ id=" + id + " ]";
  }
  
}
