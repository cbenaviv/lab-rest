/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.udea.facade;

import com.udea.model.Weather;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Daniel Jaramillo
 */
@Stateless
public class WeatherFacade extends AbstractFacade<Weather> {

  @PersistenceContext(unitName = "weatherPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public WeatherFacade() {
    super(Weather.class);
  }
  
}
